# Go Microservices with gRPC

This project is a first serious try of Go programming language, focusing on its native support for Google's gRPC protocol. It is designed as a collection of separate applications, each representing a microservice, to explore and demonstrate the efficiency of gRPC calls within a microservices architecture. The project encapsulates the fascination with Go's capabilities for building scalable, high-performance backend systems and includes a simple front-end application to interact with these microservices.

## Project Overview

The goal was to create a robust system where microservices can communicate through various methods:

- **gRPC Calls:** Leveraging Go's native gRPC support for efficient and strongly-typed service-to-service communication.
- **REST API:** A simple front-end application that interacts with the microservices through a RESTful interface.
- **RPC over gRPC:** Remote procedure calls made seamless and efficient using gRPC, facilitating clear and concise communication between services.
- **AMQP Messaging:** Utilizing Advanced Message Queuing Protocol for asynchronous messaging and work queues, ensuring reliable inter-service communication.

## Features

- **Go Microservices:** Each microservice is a separate application, showcasing Go's concurrency model and gRPC's streaming capabilities.
- **Single Entry Point:** Deployment and management of all services are simplified through a single `Makefile`, allowing for straightforward build and run processes.
- **Inter-Service Communication:** Demonstrates multiple communication strategies, including gRPC, RESTful calls, RPC over gRPC, and AMQP messaging, to provide a comprehensive overview of microservices interaction.

## Getting Started

### Prerequisites

- Go (version 1.x or later)
- Docker and Docker Compose
- RabbitMQ (for AMQP messaging)
- Any gRPC compatible client (for testing gRPC services directly)

### Setup

1. **Clone the repository**


2. **Build the project**

The entire project, including all microservices and the front-end application, can be built using the provided `Makefile`:

```bash
make all
```

3. **Run the services**

To start all services, simply use:

```bash
make run
```

This will deploy every microservice and the front-end application, making them ready to communicate with each other.
